import datetime
import functools
from flask import Flask, json, wrappers, jsonify, request, session, make_response, render_template
from flask.globals import request, session
from flask.helpers import make_response
from flask.templating import render_template
from werkzeug.exceptions import RequestURITooLarge
import jwt
from functools import wraps

app = Flask(__name__)
app.config['SECRET_KEY'] = 'Learning JWT'

def check_for_token(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return json.jsonify({'Message': 'Missing Token'}), 403
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return json.jsonify({'Message': 'Invalid token'}), 403
        return func(*args, **kwargs)
    return wrapped

@app.route("/")
def index(): 
    print('token = ', session ['token'])
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return 'Currently logged in'

@app.route('/public')
def public():
    return 'this is for public'

@app.route('/auth')
@check_for_token
def authorised():
    return 'This Private!'

@app.route('/login', methods=['POST'])
def login():
    # if request.form['username'] and request.form['password'] == 'password':
        session['logged_in'] = True 
        token = jwt.encode(
            {
                'user' : request.form['username'],
                'exp' : datetime.datetime.utcnow() + datetime.timedelta(seconds=20)
            },
            app.config['SECRET_KEY']
        )
        session['token'] = token.encode(encoding = 'UTF-8', errors = 'strict')
        print(token.encode(encoding = 'UTF-8', errors = 'strict'))
        print(type(token.encode(encoding = 'UTF-8', errors = 'strict')))
        return json.jsonify({'token':token.encode(encoding = 'UTF-8', errors = 'strict').decode('utf-8')})
    # else:
        # return make_response('Unable to Verify', 403, {'WWW-Authe+nticate': 'Basic realm: "login"'})

if __name__ == '__main__':
    app.run(debug=True)